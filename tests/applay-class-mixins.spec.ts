import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import { Base } from '../demo';

@suite
class ApplayClassMixinsSpec {

  public base: Base   = new Base();
  public name: string = 'name';

  public before(): void {
    this.base._name = this.name;
  }

  @test
  public 'set'(): void {
    this.base.name = this.name;
    expect(this.base._name).eq(this.name + this.name);
  }

  @test
  public 'get'(): void {
    expect(this.base.name).eq(this.name + this.name);
  }

  @test
  public 'method'(): void {
    this.base.clearName();
    expect(this.base._name).eq('');
  }

}
