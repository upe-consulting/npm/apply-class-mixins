export function ApplyMixins(derivedCtor: any, baseCtors: any[]): void {
  for (const baseCtor of baseCtors) {
    const propertyNames = Object.getOwnPropertyNames(baseCtor.prototype);
    for (const propertyName of propertyNames.filter((pn: string) => pn !== 'constructor')) {
      const descriptor = Object.getOwnPropertyDescriptor(baseCtor.prototype, propertyName);
      const property   = baseCtor.prototype[propertyName];
      if (!property && descriptor) {
        const derivedDescriptor =
                Object.getOwnPropertyDescriptor(derivedCtor.prototype, propertyName) || {};
        Object.defineProperty(derivedCtor.prototype, propertyName, {
          configurable: descriptor.configurable,
          enumerable:   descriptor.enumerable,
          get:          descriptor.get || derivedDescriptor.get,
          set:          descriptor.set || derivedDescriptor.set,
        });
      } else {
        derivedCtor.prototype[propertyName] = property;
      }
    }
  }
}
