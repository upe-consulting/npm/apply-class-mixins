import { ApplyMixins } from '../src/applay-class-mixins';

export interface IExtension {
  _name: string; // only required for the unit test
  clearName: () => void;
  name: string;
}

class Extension0 {

  public set name(value: string) {
    this._name = value + value;
  }

  public _name: string;

}

class Extension1 {

  public _name: string;

  public clearName(): void {
    this._name = '';
  }

}

class Extension2 {

  public get name(): string {
    return this._name + this._name;
  }

  public _name: string;

}

export class Base implements IExtension {

  // only required for the unit test
  public _name: string;

  public name: string;

  public clearName: () => void;

}

ApplyMixins(Base, [Extension0, Extension1, Extension2]);
